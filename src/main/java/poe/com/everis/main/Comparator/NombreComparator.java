package poe.com.everis.main.Comparator;

import java.util.Comparator;

public class NombreComparator  implements Comparator<Persona> {

	@Override
	public int compare(Persona per1, Persona per2) {
		System.out.println(per1.getNombre().compareTo(per2.getNombre()));
		return per1.getNombre().compareTo(per2.getNombre());
	}
	
	
	
}
